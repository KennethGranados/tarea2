/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Datos;
import Views.DatosFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author Kenneth Granados
 */
public class DatosController implements ActionListener{
    DatosFrame dFrame;
    Datos datos;
    JFileChooser jfc;

    public DatosController(DatosFrame dFrame) {
        super();
        this.dFrame =  dFrame;
        jfc = new JFileChooser();
        datos = new Datos();
    }
    
    

    @Override
    public void actionPerformed(ActionEvent ae) {
    switch(ae.getActionCommand()){
            case "save":
                jfc.showSaveDialog(dFrame);
                datos = dFrame.getDatosData();
                writeDatos(jfc.getSelectedFile());
                break;
            case "select":
                jfc.showOpenDialog(dFrame);
                datos = readDatos(jfc.getSelectedFile());
                dFrame.setDatosData(datos);
                break;
            case "clear":
                dFrame.clear();
                break;
        }
    }
    
    public void writeDatos(File file){
        try{
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(datos);
            w.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    public Datos readDatos(File file){
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Datos)ois.readObject();
            
        }
        catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
         catch(IOException|ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
