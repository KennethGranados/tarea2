
package Models;

import java.io.Serializable;

public class Datos implements Serializable{
    private String marca,modelo,vin,color,placa,fechaEmisión,chasis,tipocombustible;
    private String tipodeuso,servicio,gravamen;
    private int año,numPasajeros,numCilindros;
    private double  tonelaje;
     
    public Datos(String marca, String modelo, String vin, String color, String placa, String fechaEmisión, String chasis, String tipocombustible, String tipodeuso, String servicio, String gravamen, int año, int numPasajeros, int numCilindros, double tonelaje) {
        this.marca = marca;
        this.modelo = modelo;
        this.vin = vin;
        this.color = color;
        this.placa = placa;
        this.fechaEmisión = fechaEmisión;
        this.chasis = chasis;
        this.tipocombustible = tipocombustible;
        this.tipodeuso = tipodeuso;
        this.servicio = servicio;
        this.gravamen = gravamen;
        this.año = año;
        this.numPasajeros = numPasajeros;
        this.numCilindros = numCilindros;
        this.tonelaje = tonelaje;
    }
    
    public Datos() {
      this.marca = " ";this.modelo= " ";this.vin= " ";this.color= " ";this.placa= " ";
      this.fechaEmisión= " ";this.chasis= " ";this.tipocombustible= " ";this.servicio= " ";
      this.tipodeuso= " ";this.gravamen= " ";this.año = 0;this.numPasajeros = 0;
      this.numCilindros = 0;this.tonelaje = 0;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFechaEmisión() {
        return fechaEmisión;
    }

    public void setFechaEmisión(String fechaEmisión) {
        this.fechaEmisión = fechaEmisión;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public String getTipocombustible() {
        return tipocombustible;
    }

    public void setTipocombustible(String tipocombustible) {
        this.tipocombustible = tipocombustible;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public int getNumPasajeros() {
        return numPasajeros;
    }

    public void setNumPasajeros(int numPasajeros) {
        this.numPasajeros = numPasajeros;
    }

    public int getNumCilindros() {
        return numCilindros;
    }

    public void setNumCilindros(int numCilindros) {
        this.numCilindros = numCilindros;
    }

    public double getTonelaje() {
        return tonelaje;
    }

    public void setTonelaje(double tonelaje) {
        this.tonelaje = tonelaje;
    }

    public String getTipodeuso() {
        return tipodeuso;
    }

    public void setTipodeuso(String tipodeuso) {
        this.tipodeuso = tipodeuso;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getGravamen() {
        return gravamen;
    }

    public void setGravamen(String gravamen) {
        this.gravamen = gravamen;
    }
    
    
}
